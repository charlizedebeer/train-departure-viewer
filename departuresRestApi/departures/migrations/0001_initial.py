# Generated by Django 3.2.7 on 2021-09-11 01:15

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Departures',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('departureDirection', models.CharField(default='', max_length=70)),
                ('departureTime', models.CharField(default='', max_length=200)),
                ('departurePlatform', models.CharField(default='', max_length=70)),
                ('departureTrainType', models.CharField(default='', max_length=70)),
            ],
        ),
    ]
