from django.db import models


class Departures(models.Model):
    departureDirection = models.CharField(max_length=70, blank=False, default='')
    departureTime = models.CharField(max_length=200,blank=False, default='')
    departurePlatform = models.CharField(max_length=70, blank=False, default='')
    departureTrainType = models.CharField(max_length=70, blank=False, default='')